function vol_slic(id,w,h)
whichdata = 'spie-dataset';

Data = dir(whichdata);
Data = Data(arrayfun(@(x) x.name(1), Data) ~= '.');
filepath = fullfile(whichdata,Data(id).name);
savepath= fullfile('3D-labels-gen',Data(id).name);
if exist(savepath,'file')
    disp('Label exists, skipping..')
    return
end
load(filepath);

img = scan.img;
loc = scan.idx;
p1 = 5;
p2 = 1*1e6;
disp(loc)
segments = zeros(size(img));
params.small = 20;
params.long = 50;
params.pixel = 400;
params.ratio = 0.1;
params.length = 6;
for i = 1:2*h+1
    I = img(loc(2)-w:loc(2)+w,loc(1)-w:loc(1)+w,loc(3)-h-1+i);
    %imask = im_filter_seg(I,15,10);
    imask = ones(size(I));
    segs = zeros(size(img(:,:,1)));
    im = single(I)>500;
    imz = im.*imask;
    tmp= vl_slic(single(I),p1,p2);
    labimg = super_pixel_features(single(I),tmp,imz,imask,params);
    
    %segs(loc(2)-w:loc(2)+w,loc(1)-w:loc(1)+w) = labimg;
    segments(loc(2)-w:loc(2)+w,loc(1)-w:loc(1)+w,loc(3)-h-1+i) = labimg;
% 
%     subplot(221),imagesc(single(I).*imz)
%     subplot(222),imagesc(labimg)
%     subplot(223),imagesc(drawregionboundaries(tmp, single(I),5000))
%     subplot(224),imagesc(I)
%      pause()
end
tmp = smooth3(segments,'gaussian');
segments = tmp>0;
save(savepath,'segments');
for j = 1:2*h-1
subplot(211),imagesc(segments(:,:,loc(3)-h+j));
subplot(212),imagesc(img(:,:,loc(3)-h+j));
pause(.1)
end


