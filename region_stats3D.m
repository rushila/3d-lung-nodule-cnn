function [newstats,L] = region_stats3D(p)
cc = bwconncomp(p>0.4,6);
L = labelmatrix(cc);
stats = regionprops(cc, 'area', 'centroid','pixellist');
idx1 = find([stats.Area]>20000);
idx2 = find([stats.Area]<100);
stats([idx1 idx2]) = [];
newstats = stats;

% h = [stats.Centroid];
% h = reshape(h,3,length(stats));
% [centroids,labels] = kmeans(h,400);
% newstats = centroids;
%newstats.Label = labels;