function  preds3D = prediction_main(id)

whichdir = '3D-proposals';
Data = dir(whichdir);
Data = Data(arrayfun(@(x) x.name(1), Data) ~= '.');
filename = Data(id).name;
savedir = 'final-result';

if exist(fullfile(savedir,filename),'file')
    preds3D = 0;
    disp('prediction exists!')
    return
end

tic
p1 = run_detection(id,25,04,false);
%p1 = smooth3(p1tmp,'gaussian',5);

%p2tmp = run_detection(id,10,10,false);
%p2 = smooth3(p2tmp,'gaussian',5);

p3 = run_detection(id,12,03,false);
%p3 = smooth3(p3tmp,'gaussian',5);
toc

p = (p1+p3)/2;
preds3D = smooth3(p,'gaussian',5);
%preds3D = p1;
savepath = fullfile(savedir,filename);
save(savepath,'preds3D');
