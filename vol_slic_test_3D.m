function vol_slic_test_3D(id)
N = length(id);
p1 = 1e3;
p2 = 10;
Data = dir('scan_test_proc/');
Data = Data(arrayfun(@(x) x.name(1), Data) ~= '.');

for n= 1:N
    filepath = fullfile('scan_test_proc',Data(id(n)).name);
    CT = load(filepath);
    img = CT.scan.img;
    loc = CT.scan.idx;
    
    disp(loc)
    imz = zeros(size(img));
    
    parfor i = 1:size(img,3)-80
        I = img(:,:,i+40);
        im = single(I)>500;
        imask = im_filter_seg(I,20,15);
        imz(:,:,i+40) = im.*imask;
        %imz(:,:,i+40) = im;
    end
    tens = single(img).*imz;
    
    [tmp,~] = slicsupervoxelmex(tens,p1,p2);
    
    % tmp = vl_slic(single(tens),p1,p2);
    segments = super_pixel_features3D(tens,tmp,imz);
    disp('Computed superpixel features, saving..')
    save(fullfile('3D-spx-prop',Data(id(n)).name),'segments');
end
