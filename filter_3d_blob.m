function filter_3d_blob()

whichdir = '3D-proposals';
Data = dir(whichdir);
Data = Data(arrayfun(@(x) x.name(1), Data) ~= '.');
savedir = '3D-proposals2';

for n = 1:length(Data)
    n
    filename = Data(n).name;
    load(fullfile(whichdir,filename));
    cc = bwconncomp(segments>0,6);
    stats = regionprops(cc, 'area', 'centroid','pixellist');
    A = [stats.Area];
    px = cc.PixelIdxList;
    px(A<100) = [];
    newC = cc;
    newC.NumObjects = length(px);
    newC.PixelIdxList = px;
    L = labelmatrix(newC);
    segments = L;
    save(fullfile(savedir,filename),'segments');
    
end