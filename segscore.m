function [tp,fp] = segscore(p,loc,thresh)

stats = regionprops(p>thresh, 'area', 'centroid');

idx1 = find([stats.Area]<10);
idx2 = find([stats.Area]>600);

stats([idx1 idx2]) = [];
A = reshape([stats(:).Centroid]',3,length(stats));
x = bsxfun(@minus,A,loc');
y = sqrt(sum(x'.^2,2));
[val,id] = min(y);

if val<30
    tp = 1;
else 
   tp = 0;
end
    
cp = stats;
cp(id) = [];
fp = length(cp);