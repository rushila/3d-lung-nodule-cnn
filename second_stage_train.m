function model = second_stage_train()
Data = dir('scan_proc/');
Data = Data(arrayfun(@(x) x.name(1), Data) ~= '.');
N = length(Data);
H = [];
L = [];

for n = 1:N
filepath = fullfile('spx-features-train',Data(n).name);
CT = load(filepath);
H = [H CT.segments];
L = [L CT.labels];
end
H2 = bsxfun(@minus,H,mean(H,2));
%
model = mnrfit(H2',L'+1);
save('annotated_data/Model-pretrain/MNR_model.mat');



