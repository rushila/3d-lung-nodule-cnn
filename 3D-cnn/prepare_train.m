function [xtrain3d, L] = prepare_train(tr_id,w,h)
filedir = fullfile('annotated_data','Train',[num2str(w),'-',num2str(h)]);
Datapath = dir(filedir);
Datapath = Datapath(arrayfun(@(x) x.name(1), Datapath) ~= '.');
Data = Datapath(tr_id);
feats = cell(1,length(Data));
labels = cell(1,length(Data));
num = (2*w+1)*(2*w+1)*(2*h+1);

for id = 1:length(Data)
    filename = Data(id).name;
    disp(filename)
    filepath = fullfile(filedir,filename);
    D = load(filepath);
    feats{id} = single(D.data.feats);
    labels{id} = D.data.label;
end
L = cell2mat(labels);
x = feats{1}(:,:,:,1);
xtrain3d = single(zeros([num,length(L)]));
start = 1;

for i = 1:length(Data)
    tic
    l = size(feats{i},2);
    stop = l+start-1;
    xtrain3d(:,start:stop) = feats{i};
    start = stop+1;
    toc
end



