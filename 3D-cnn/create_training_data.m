function create_training_data(tr_id,w,h)
%whichdata = 'scan-proc';
whichdata = 'scan_train_proc';
Data = dir(whichdata);
Data = Data(arrayfun(@(x) x.name(1), Data) ~= '.');
trData = Data(tr_id);
savedir = fullfile('annotated_data','Train',[num2str(w),'-',num2str(h)]);
mkdir(savedir)
for id = 1:length(trData)
    filename = trData(id).name;
    disp(filename)
    filepath = fullfile(whichdata,filename);
    load(filepath);
    savepath = fullfile(savedir,trData(id).name);
    if exist(savepath,'file')
        disp('training data exists, continuing..')
        continue
    end
    img = scan.img;
    loc = scan.idx;
    labelpath = fullfile('3D-labels-gen',filename);
    load(labelpath);
    
    [tr_feats, labels] = features_per_scan(img,segments,w,h,loc);
    data.feats = single(tr_feats);
    data.label = labels;
    
    save(savepath,'data','-v7.3');
end
