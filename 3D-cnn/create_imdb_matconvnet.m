function images = create_imdb_matconvnet(training_data,labels)

N = size(training_data,2);
randidx = randperm(N,N);
y = labels;
z = training_data;
r = floor(0.90*N);
v1 = [ones(1,r) 2*ones(1,N-r)];
v = v1(randidx);


db = struct(...
    'id',1:N,...
    'data',z,...
    'label',y+1,...
    'set',v...
    );

images = db;
% save('annotated_data/Train/cnn_db.mat','images');