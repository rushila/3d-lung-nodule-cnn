function net = custom_cnn_train(data,varargin)
setup;
%imdb = load('annotated_data/Train/cnn_db.mat') ;
imdb.images = data;
net = initializeCNN_25_4() ;

% -------------------------------------------------------------------------
% Part 4.3: train and evaluate the CNN
% -------------------------------------------------------------------------

trainOpts.batchSize = 100 ;
trainOpts.numEpochs = 15 ;
trainOpts.continue = true ;
trainOpts.useGpu = false ;
trainOpts.learningRate = 0.001 ;
trainOpts.expDir = 'annotated_data/Model-25-04/nodule-expt' ;
trainOpts = vl_argparse(trainOpts, varargin);

% Take the average image out
%imdb = load('annotated_data/Train/cnn_db.mat') ;
imageMean = mean(imdb.images.data(:)) ;
imdb.images.data = imdb.images.data - imageMean ;

% Convert to a GPU array if needed
if trainOpts.useGpu
  imdb.images.data = gpuArray(imdb.images.data) ;
end

% Call training function in MatConvNet
[net,info] = cnn_train(net, imdb, @getBatch, trainOpts) ;

% Move the CNN back to the CPU if it was trained on the GPU
if trainOpts.useGpu
  net = vl_simplenn_move(net, 'cpu') ;
end

% Save the result for later use
net.layers(end) = [] ;
net.imageMean = imageMean ;
save('annotated_data/Model-25-04/nodulecnn.mat', '-struct', 'net') ;

% -------------------------------------------------------------------------
% Part 4.4: visualize the learned filters
% -------------------------------------------------------------------------

% figure(2) ; clf ; colormap gray ;
% vl_imarraysc(squeeze(net.layers{1}.filters),'spacing',2)
% axis equal ; title('filters in the first layer') ;

% -------------------------------------------------------------------------
% Part 4.6: train with jitter
% -------------------------------------------------------------------------

% trainOpts.batchSize = 100 ;
% trainOpts.numEpochs = 15 ;
% trainOpts.continue = true ;
% trainOpts.learningRate = 0.001 ;
% trainOpts.expDir = 'data/nodule-jit-experiment' ;
% 
% % Initlialize a new network
% net = initializeCharacterCNN() ;
% 
% % Call training function in MatConvNet
% [net,info] = cnn_train(net, imdb, @getBatchWithJitter, trainOpts) ;
% 
% % Move the CNN back to CPU if it was trained on GPU
% if trainOpts.useGpu
%   net = vl_simplenn_move(net, 'cpu') ;
% end
% 
% % Save the result for later use
% net.layers(end) = [] ;
% net.imageMean = imageMean ;
% save('data/nodulecnn-jit.mat', '-struct', 'net') ;
% --------------------------------------------------------------------
function [im, labels] = getBatch(imdb, batch)
% --------------------------------------------------------------------
im = imdb.images.data(:,batch) ;
im = 1 * reshape(im, 51, 51, 9, []) ;
labels = imdb.images.label(1,batch) ;

% --------------------------------------------------------------------
function [im, labels] = getBatchWithJitter(imdb, batch)
% --------------------------------------------------------------------
im = imdb.images.data(:,:,batch) ;
labels = imdb.images.label(1,batch) ;

n = numel(batch) ;
train = find(imdb.images.set == 1) ;

sel = randperm(numel(train), n) ;
im1 = imdb.images.data(:,:,sel) ;

sel = randperm(numel(train), n) ;
im2 = imdb.images.data(:,:,sel) ;

ctx = [im1 im2] ;
ctx(:,17:48,:) = min(ctx(:,17:48,:), im) ;

dx = randi(11) - 6 ;
im = ctx(:,(17:48)+dx,:) ;
sx = (17:48) + dx ;

dy = randi(5) - 2 ;
sy = max(1, min(32, (1:32) + dy)) ;

im = ctx(sy,sx,:) ;

% Visualize the batch:
% figure(100) ; clf ;
% vl_imarraysc(im) ;

im = 1 * reshape(im, 21, 21, 1, []) ;



