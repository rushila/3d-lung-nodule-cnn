function [xtest3d,label] = prepare_test_img(im)

m = zeros(size(im));
w = 10;
imbw = im_filter_seg(im);
[x_train] = feature_per_pixel_full(single(im).*imbw,m,w);
label = x_train(:,end);
x_train(:,end) = [];

x_train = bsxfun(@minus,x_train,mean(x_train,1));
s = sqrt(length(x_train(1,:)));
xtest3d = reshape(x_train',s,s,[]) ;