function te_feats = features_per_superpixel_test(vol,mask,w,d,varargin)
U = unique(mask);

te_feats = zeros(2*w+1,2*w+1,2*d+1,length(U)-1);
Y = mask(:);
[S, iSorted] = sort(Y);
markers = [0; find(diff(S)); numel(Y)];

[R,C,D] = size(mask);
parfor i = 2:numel(markers)-1
    idx = iSorted(markers(i)+1:markers(i+1));
    [r1,c1,t1] =ind2sub(size(mask),idx);
    idx1 = (t1>d+2)&(t1<D-d-2);
    idx2 = (c1>w+2)&(c1<C-w-2);
    idx3 = (r1>w+2)&(r1<R-w-2);
    idx = idx1&idx2&idx3;
    r = round(mean(r1(idx)));c = round(mean(c1(idx)));t = round(mean(t1(idx)));
    box = vol(r-w:r+w,c-w:c+w,t-d:t+d);
    te_feats(:,:,:,i-1) = box;
    
end



if ~isempty(varargin)
    filename = sprintf('%s',varargin{1});
    filepath = fullfile('annotated_data','Test',filename);
    save(filepath,'te_feats','-v7.3');
end