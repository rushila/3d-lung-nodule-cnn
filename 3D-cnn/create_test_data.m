function te_feats = create_test_data(id,w,h,isFinal)
whichpath = '3D-proposals';
Data = dir(whichpath);

Data = Data(arrayfun(@(x) x.name(1), Data) ~= '.');

filename = Data(id).name;
disp(filename)
filepath = fullfile('scan_test_proc',filename);
CT = load(filepath);
disp(CT.scan.idx)
labelpath = fullfile(whichpath,filename);
load(labelpath);
if isFinal
    segments = gen_proposal(segments,filename);
end
te_feats = features_per_superpixel_test(CT.scan.img,segments,w,h,filename);


