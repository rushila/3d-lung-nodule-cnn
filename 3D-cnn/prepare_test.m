function [xtest3d, label] = prepare_test(id)
filename = sprintf('annotated_data/Test/test-features-%.3d.csv',id);
%filename = 'annotated_data/Train/training.csv';
x_train = load(filename);
label = x_train(:,end);
x_train(:,end) = [];
x_train = bsxfun(@minus,x_train,mean(x_train,1));
s = sqrt(length(x_train(1,:)));
xtest3d = reshape(x_train',s,s,[]) ;

