function [out] = feature_per_pixel_full(im,m,w)
[R,C] = size(im);

%im = im/max(max(im));
disp('calculating features per pixel  \n');
i = 1;
CIDX = w+1:1:C-w-1;
k = length(CIDX);
N = (2*w+1)^2;
h = zeros(k,N);
volfeat = zeros((R - 2*w-2)*k,N);
for r = w+1:1:R-w-1
    winx = r-w:r+w;

    fprintf('\b\b\b\b\b %04.1f',r*100/(R-w-1));
    parfor c = 1:k
        winy = CIDX(c)-w:CIDX(c)+w;
        vol = im(winx,winy);
        hogf = vol(:)';
        h(c,:) = double(hogf);
    end
    volfeat((i-1)*k+1:i*k,:) = h;
    i = i+1;
    
end
m1 = m(w+1:1:R-w-1,w+1:1:C-w-1)';
m2 = m1(:);
% pos = find(m2>0);
% neg = 1:length(m2);
% neg(pos) = [];
% idxneg = neg(randperm(length(neg),T));
volfeat(:,end+1) = m2;
out = volfeat;
% idx = [pos' idxneg]';
% out = volfeat(idx,:);

