function [te_feats,label] = features_per_scan_test(vol,mask,w,h,varargin)
M = 30000;
mask(450:end,:,:) = 0;
mask(:,450:end,:) = 0;

mask(1:80,:,:) = 0;
mask(:,1:120,:) = 0;
s = logical(mask(:,:,10));
ind = find(s);
[R,C,D] = size(mask);

[r1, c1, d1] = ind2sub([R,C,D], ind);
r = r1;
c = c1;
%r = r1((r1>4*w)&(r1<R-4*w));
%c = c1((c1>4*w)&(c1<C-4*w));
%d = d((d>2*h)&(d<D-2*h));
d = 0.5*(D+1);

%val = min([length(r),length(c),length(d)]);
val = min([length(r),length(c)]);
N = val;

paramfile = sprintf('param_%s',varargin{1});
param.val = val;
param.row = r;
param.col = c;
%save(fullfile('annotated_data','Test',paramfile),'param');
te_feats = zeros(2*w+1,2*w+1,2*h+1,M);
count = 1;
tic
for n = 1:N
    %box = vol(r(n)-w:r(n)+w,c(n)-w:c(n)+w,d(n)-h:d(n)+h);
    box = vol(r(n)-w:r(n)+w,c(n)-w:c(n)+w,d-h:d+h);
    label(count) = 0;
    te_feats(:,:,:,count) = box;
    if ~mod(n,M)
        toc
        disp(n)
        filename = sprintf('%.7d_%s',n,varargin{1});
        filepath = fullfile('annotated_data','Test',filename);
        if ~exist(filepath,'file');
            save(filepath,'te_feats','label','param','-v7.3');
        end
        count = 0;
        tic
    end
    count = count+1;
    
end
te_feats = te_feats(:,:,:,1:count-1);
filename = sprintf('%.7d_%s',count,varargin{1});
filepath = fullfile('annotated_data','Test',filename);
save(filepath,'te_feats','label','param','-v7.3');