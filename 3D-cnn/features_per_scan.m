function [tr_feats,label] = features_per_scan(scan,proposal,w,h,loc)

vol = scan;
s = logical(proposal);
F = 15;
% if bwarea(s(:,:,loc(3)))> 200
%     F = 20;
% else
%     F = 15;
% end

ind = find(s);
[i1, i2, i3] = ind2sub(size(s), ind);
N = length(ind);
num = (2*w+1)*(2*w+1)*(2*h+1);
tr_feats_pos = zeros(num,N);

parfor n = 1:N
    
    box = vol(i1(n)-w:i1(n)+w,i2(n)-w:i2(n)+w,i3(n)-h:i3(n)+h);
    box_label(n) = 1;
    tr_feats_pos(:,n) = box(:);
    
end
tmp = vol(2*w:end-2*w,2*w:end-2*w,20:end-20);

vol2 = remove_lungs(tmp,F);
sm = s(2*w:end-2*w,2*w:end-2*w,20:end-20);
I = vol2(:,:,loc(3)-20);
if ~nnz(I)
    vol2 = remove_lungs(tmp,5);
end
subplot(211),imagesc(vol2(:,:,loc(3)-20));
subplot(212),imagesc(tmp(:,:,loc(3)-20));
pause(0.1)
allind = find((vol2>0)&(~sm));

K = length(allind);
negidx = randperm(K,N);
[R,C,D] = size(sm);

negind = allind(negidx);
[r, c, d] = ind2sub(size(sm), negind);

idx1 = (d>h+2)&(d<D-h-2);
idx2 = (c>w+2)&(c<C-w-2);
idx3 = (r>w+2)&(r<R-w-2);
idx = idx1&idx2&idx3;

 r = r(idx);
 c = c(idx);
 d = d(idx);
 
tr_feats_neg = zeros(num,length(d));

parfor k = 1:length(d)
    boxn = vol2(r(k)-w:r(k)+w,c(k)-w:c(k)+w,d(k)-h:d(k)+h);
    boxn_label(k) = 0;
    if ~nnz(boxn)
        error('Lung segmentation failed')
    end
    tr_feats_neg(:,k) = boxn(:);
end

tr_feats = cat(2,tr_feats_pos,tr_feats_neg);
label = [box_label boxn_label];