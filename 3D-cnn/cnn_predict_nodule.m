function preds = cnn_predict_nodule(net,feats)
d4 = size(feats,4);

preds = zeros(1,d4);


for i = 1:d4
    res = vl_simplenn(net, single(feats(:,:,:,i))) ;
    y = squeeze(res(end).x);
    p = clean_up_predictions(y);
    preds(i) = p(2);
end

