function preds = gen_prob_map(id,preds,isFinal)

whichdir = '3D-proposals';
Data = dir(whichdir);
Data = Data(arrayfun(@(x) x.name(1), Data) ~= '.');
filename = Data(id).name;
labelpath = fullfile(whichdir,filename);
load(labelpath);
if isFinal
    mask = gen_proposal(segments,filename);
else mask = segments;
end

T = size(mask,3);

for i = 1:T
    S{i} = unique(mask(:,:,i))';
    h(i) = length(S{i});
end

F = 1:T;
F(h<2) = [];

preds3D = zeros(size(mask));

count = 1;


for n = 1:length(F)
    Y = zeros(size(mask(:,:,1)));
    M = mask(:,:,F(n));
    s = S{F(n)};
    s(1) = []; %background
    
    for m = 1:length(s)
        idx = M == s(m);
        Y(idx) = preds(count);
        count = count+1;
    end
   preds3D(:,:,F(n)) = Y; 
end
preds = preds3D;


