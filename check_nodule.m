clc
clear
whichdir = 'final-results';
D = dir(whichdir);
D = D(arrayfun(@(x) x.name(1), D) ~= '.');
N = length(D);

parfor n = 1:N
    n
    filename = D(n).name;
    A = load(fullfile('scan_test_proc',filename));
    B = load(fullfile(whichdir,filename));
    preds3D = B.preds3D;%/max(preds3D(:));
    loc = A.scan.idx;
    tmp = preds3D(loc(2)-5:loc(2)+5,loc(1)-5:loc(1)+5,loc(3)-1:loc(3)+1);
    p(n,:) = tmp(:);
end