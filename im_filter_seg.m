function imbw = im_filter_seg(img,t)

se2 = strel('disk',t);

im = imopen(img,se2);
level = graythresh(im);
imsk = im2bw(im,level);
%imsk = im >400;
im2 = imfill(imsk,'holes');
im3 = im2+imsk;

imbw1 = xor(imsk,im2);
filled = imfill(imbw1,'holes');
holes = filled & ~imbw1;
bigholes = bwareaopen(holes, 1000);
smallholes = holes & ~bigholes;
imbw = xor(imbw1,smallholes);


if (nnz(imbw) <150) && (nnz(imbw) >50)
    disp('WARNING:Unable to segment lungs!')
    %imbw = ~imsk;
end



% b = regionprops(immask1,'PixelList','Area');
% a = nestedSortStruct(b,'Area');
% 
% a(end) = [];
% 
% 
% m1 = zeros(size(im));
% for i = 1:length(a)
%     id = a(i).PixelList;
%     m1(id(:,2),id(:,1)) = 1;
% end
%  imbw = m1.*immask1;
% imagesc(imbw),colormap autumn

