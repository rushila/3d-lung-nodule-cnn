function out = super_pixel_features3D(img,segs,varargin)
tic
imz= varargin{1};
segs2 = single(segs).*imz;
Y = segs2(:);
Z = img(:);
% 
% idx = Y==0;
% Y(idx) = [];
% Z(idx) = [];

[S, iSorted] = sort(Y);
markers = [0; find(diff(S)); numel(Y)];
bins = 10;
h = zeros(bins,numel(markers)-2);
mnrmodel = load('annotated_data/Model-pretrain/model.mat');

Z1 = zeros(1,length(Z));

parfor i = 2:numel(markers)-1
    
    f = Z(iSorted(markers(i)+1:markers(i+1)));
    h(:,i-1) = hist(f,bins);
end

h = bsxfun(@minus,h,mean(h,2));
tmp = mnrval(mnrmodel.model,h');
conf = tmp(:,2)';

for i = 2:numel(markers)-1    
    Z1(iSorted(markers(i)+1:markers(i+1))) = conf(i-1);
   
end
toc
Y(Z1<0.30) = 0;
out = reshape(Y,size(segs));


