function scan = extract_3d_vol()
load test_locs.mat;
%load('train_locs.mat');
data_path = 'test_scans';
%data_path = 'datasets/DOI';
addpath('../nestedSortStruct/');
%load train_locs.mat
im_c = 512; im_r = 512; %size of each CT slice

Data = dir(data_path);
Data = Data(arrayfun(@(x) x.name(1), Data) ~= '.');

for i = 1:size(L,1)
    %subj = Data(i).name;
    subj = ScanNumber{i};
    str = sprintf('Processing lung scan # %d %s \n',i,subj);
    disp(str)
    subj_tmp = dir(fullfile(data_path,subj));
    subj_tmp = subj_tmp(arrayfun(@(x) x.name(1), subj_tmp) ~= '.');
%     if exist(['scan_test_proc/',subj,'.mat'],'file')
%         continue
%     end
    subj_path = fullfile(data_path,subj,subj_tmp(1).name);
    subj_dir_tmp = dir(subj_path);
    subj_dir_tmp = subj_dir_tmp(arrayfun(@(x) x.name(1), subj_dir_tmp) ~= '.');
    
    img_path = fullfile(subj_path,subj_dir_tmp(1).name);
    img_dir = dir(img_path);
    img_dir = img_dir(arrayfun(@(x) x.name(1), img_dir) ~= '.');
    T = length(img_dir);
    parfor n = 1:T
        I = dicomread(fullfile(img_path,img_dir(n).name));
        info = dicominfo(fullfile(img_path,img_dir(n).name));
        DCM(n).img = I;
        DCM(n).slice = info.InstanceNumber;
    end
    s = nestedSortStruct(DCM,'slice');
    idx = L(i,3);
    for j = 1:length(s)
        scn(:,:,j) = s(j).img;
    end
    scan.img = scn;
    scan.idx = L(i,:);
    save(fullfile('scan_test_proc',[subj,'_',num2str(L(i,3))]),'scan');
    clear scn s I DCM
end



