function out = super_pixel_features(vol,segs,varargin)
params = varargin{3};

if length(varargin)>3
    global ctr
    incrementCounter = true;
else
    incrementCounter = false;
    ctr = 1;
end

scan = (vol).*single(varargin{1});
imask = varargin{2};
img = zeros(size(scan));
segs = single(segs);
scant = scan>500;
segs2 = segs.*imask.*scant;

Y = segs2(:);
Z = scan(:);

[S, iSorted] = sort(Y);
markers = [0; find(diff(S)); numel(Y)];



for i = 2:numel(markers)-1
    idx = iSorted(markers(i)+1:markers(i+1));
    [r,c] = ind2sub(size(img),idx);
    f = Z(idx);
    isTooLong = (max(r)-min(r)>params.length)||(max(c)-min(c)>params.length);
    isTooSmall = (length(f)<params.small);
    isTooLarge = (length(f)>params.long);
    isBright = (sum(f > params.pixel)/length(f)) >params.ratio;
    
    if  isBright && ~isTooSmall &&~isTooLarge  && ~isTooLong
        img(idx) = ctr;
        if incrementCounter
            ctr = ctr+1;
        end
    end
    
end
bw1 = bwareafilt(img>0,[10,1000]);
out = bw1.*img;