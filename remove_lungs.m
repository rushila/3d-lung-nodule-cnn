function newScan = remove_lungs(scan,F)

T = size(scan,3);

parfor t = 1:T
    I = scan(:,:,t);
    imask = im_filter_seg(I,F);
    newScan(:,:,t) = uint16(imask).*I;
end
