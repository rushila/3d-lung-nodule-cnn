function te_feats = features_per_superpixel_test1(vol,mask,w,d,varargin)
T = size(mask,3);
S = unique(mask);
S(1) = [];
te_feats = zeros(2*w+1,2*w+1,2*d+1,numel(S));


parfor n = 1:length(S)
    tic
    idx = find(mask == S(n));
    [r1,c1,d1] =ind2sub(size(vol),idx);
    tmp = median([r1 c1 d1]);
    r = floor(tmp(1));c = floor(tmp(2));p = floor(tmp(3));
    box = vol(r-w:r+w,c-w:c+w,p-d:p+d);
    te_feats(:,:,:,n) = box;
    toc
end


if ~isempty(varargin)
    filename = sprintf('%.7d_%s',length(S),varargin{1});
    filepath = fullfile('annotated_data','Test',filename);
    save(filepath,'te_feats','-v7.3');
end