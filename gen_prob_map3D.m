function preds3D = gen_prob_map3D(id,preds)

whichdir = '3D-spx-prop';
Data = dir(whichdir);
Data = Data(arrayfun(@(x) x.name(1), Data) ~= '.');
filename = Data(id).name;
labelpath = fullfile(whichdir,filename);
load(labelpath);
mask = segments;
preds3D = zeros(size(mask));

Y = mask(:);
[S, iSorted] = sort(Y);
markers = [0; find(diff(S)); numel(Y)];


tic
for i = 2:numel(markers)-1
      idx = iSorted(markers(i)+1:markers(i+1));
      preds3D(idx) = preds(i-1);
end
toc

% for n = 2:length(S)
%    idx = mask == S(n);
%    preds3D(idx) = preds(n-1); 
% end
