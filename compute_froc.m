function [TPA,FNA,FPA]= compute_froc()
%rng('default');
whichdir = 'final-result-1';
D = dir(whichdir);
D = D(arrayfun(@(x) x.name(1), D) ~= '.');
count = 1;
N = length(D);

load('test_3d_locs.mat');
loc = round(loc);
ids = 1:N;
batch = 1;
batchsize = N;
%ids = ids(randperm(N));

for k = 1:batch
    
    %teids = ids(randperm(N,batchsize));
    teids = ids;
    if ~exist('detections.mat','file')
        for i = teids
            tic
            filename = D(i).name;
            p = load(fullfile(whichdir,filename));
            disp(filename)
            p2 = p.preds3D;
            [stats,LabelMat] = region_stats3D(p2);
            
            for j = 1:length(stats)
                L = round(stats(j).Centroid);
                A(count,1:3) = L;
                subs = [stats(j).PixelList];
                ind = sub2ind(size(p2),subs(:,2),subs(:,1),subs(:,3));
                vals = p2(ind);
                A(count,4) = max(vals);
                A(count,5) = i;
                A(count,6) = norm(L-loc(i,:));
                dis(j) = norm(L-loc(i,:));
                count = count+1;
            end
            [mindis(i),idx] = min(dis);
            area(i) = stats(idx).Area;
            disp(mindis(i));
            clear dis
            toc
        end
        preds{k} = A;
    else
        load('detections.mat');
        A = preds{k};
    end
    
    %%
    b = A(:,4);
    y = sort(b,'descend');
    numFP = 10;
    %val = y(length(teids)*numFP);
    val = 0.6;
    B = A(A(:,4)>=val,:);
    ctr = 1;
    neighb = 30;
    for thresh = val:0.005:0.65
        Ctmp = B(B(:,6)<neighb,:);
        C = Ctmp(Ctmp(:,4)>thresh,:);
        tp(ctr) = length(unique(C(:,5)));
        fn(ctr) = length(D)-tp(ctr);
        Btmp = B(B(:,6)>neighb,4);
        fp(ctr) = nnz(Btmp>=thresh);
        ctr = ctr+1;
    end
    plot(fp/length(teids),tp/length(teids),'o-');
    hold on
    pause(0.1)
    TPA(k,:) = tp/length(teids);
    FPA(k,:) = fp/length(teids);
    FNA(k,:) = fn/length(teids);
    clear tp fp fn A
end
grid on
save('detections.mat','preds','mindis','area');
fpm = mean(FPA);
fps = std(FPA);
tpm = mean(TPA);
tps = std(TPA);
%figure,errorbar(fpm,tpm,tps)

%axis([0 numFP 0 1]);

