function loc = compute_3D_label_centroids()
whichdir = 'final-result';
D = dir(whichdir);
D = D(arrayfun(@(x) x.name(1), D) ~= '.');
count = 1;
N = length(D);
for n = 1:N
    filename = D(n).name;
    load(fullfile('3D-labels-gen',filename));
    cc = bwconncomp(segments,6);
    stats = regionprops(cc, 'area', 'centroid','pixellist');
    j = [stats.Area];
    [val,ind] = max(j);
    loc(n,:) = stats(ind).Centroid;
end


save('test_3d_locs.mat','loc');