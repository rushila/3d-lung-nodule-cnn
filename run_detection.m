function preds3D = run_detection(id,w,h,isFinal)

whichModel = sprintf('Model-%.2d-%.2d',w,h);
te_feats = create_test_data(id,w,h,isFinal);
net = load(['annotated_data/',whichModel,'/nodulecnn.mat']);
preds = cnn_predict_nodule(net,te_feats);
preds3D = gen_prob_map(id,preds,isFinal);

