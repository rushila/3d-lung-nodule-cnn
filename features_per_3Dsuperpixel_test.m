function te_feats = features_per_3Dsuperpixel_test(vol,mask,w,d,varargin)
T = size(mask,3);
U = unique(mask);

te_feats = zeros(2*w+1,2*w+1,2*d+1,length(U)-1);

count = 1;

Y = mask(:);
[S, iSorted] = sort(Y);
markers = [0; find(diff(S)); numel(Y)];
tic
for i = 2:numel(markers)-1
    
    idx = iSorted(markers(i)+1:markers(i+1));
    [r1,c1,t1] =ind2sub(size(mask),idx);
    tmp = median([r1 c1 t1]);
    r = floor(tmp(1));c = floor(tmp(2));t = floor(tmp(3));
    box = vol(r-w:r+w,c-w:c+w,t-d:t+d);
    te_feats(:,:,:,i-1) = box;
    
end
toc
%
% for m = 2:length(S)
%     tic
%     idx = find(mask == S(m));
%     [r1,c1,t1] =ind2sub(size(mask),idx);
%     tmp = median([r1 c1 t1]);
%     r = floor(tmp(1));c = floor(tmp(2));t = floor(tmp(3));
%     box = vol(r-w:r+w,c-w:c+w,t-d:t+d);
%     te_feats(:,:,:,count) = box;
%     count = count+1;
%     toc
% end




if ~isempty(varargin)
    filename = sprintf('%s',varargin{1});
    filepath = fullfile('annotated_data','Test',filename);
    save(filepath,'te_feats','-v7.3');
end