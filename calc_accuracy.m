function [prec,sens] = calc_accuracy()
whichdir = 'final-results';
D = dir(whichdir);
D = D(arrayfun(@(x) x.name(1), D) ~= '.');
count = 1;
for thresh = 0.05:0.05:0.7
    disp(thresh)
    parfor i = 1:length(D)
        filename = D(i).name;
        p = load(fullfile(whichdir,filename));
        scan = load(fullfile('scan_test_proc',filename));
        loc = scan.scan.idx;
        [tp,fp] = segscore(p.preds3D,loc,thresh);
        T(i) = tp;
        F(i) = fp;
    end
    prec(count) = sum(T)/(sum(T)+0.1*sum(F));
    sens(count) = sum(T)/(length(D));
    count = count+1;
end