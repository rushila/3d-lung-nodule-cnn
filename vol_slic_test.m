function vol_slic_test(id)
N = length(id);
global ctr;
count = 0;

params.small = 20;
params.long = 150;
params.pixel = 500;
params.ratio = 0.4;
params.length = 15;
for n = 1:N
    
    Data = dir('scan_test_proc/');
    Data = Data(arrayfun(@(x) x.name(1), Data) ~= '.');
    savepath = fullfile('3D-proposals',Data(id(n)).name);
    filepath = fullfile('scan_test_proc',Data(id(n)).name);
    disp(Data(id(n)).name)
    if exist(savepath,'file')
        continue
    end
    CT = load(filepath);
    
    img = CT.scan.img;
    loc = CT.scan.idx;
    p1 = 5;
    p2 = 1e5;
    disp(loc)
    ctr = 1;
    segments = zeros(size(img));
    
    for i = 1:size(img,3)-80
        %for i = loc(3):loc(3)
        
        I = img(:,:,i+40);
        im = single(I)>500;
        imask = im_filter_seg(I,20);
        if nnz(imask)/numel(imask)<0.08;
            imask = im_filter_seg(I,5);
        end
        tmp = vl_slic(single(I),p1,p2);
        imz = im.*imask;
        labimg = super_pixel_features(single(I),tmp,imz,imask,params,ctr);
        segments(:,:,i+40) = labimg;
        
    end
    
    z = nnz(imask(loc(2)-8:loc(2)+8,loc(1)-8:loc(1)+8))>0;
    
    x = segments(loc(2)-8:loc(2)+8,loc(1)-8:loc(1)+8,loc(3));
    
    y = nnz(x)> 0;
    
    if y
        fprintf('\n Captured Nodule: %d, Sparsity: %.4f\n',y,nnz(segments)/numel(segments));
        count = count+1;
        save(savepath,'segments');
    elseif z
        fprintf('\n Lung segmentation captured nodule\n');
        disp(id(n))
    else
        fprintf('\n Missed Nodule!\n');
        save(savepath,'segments');
        disp(id(n))
        
    end
    
end
count