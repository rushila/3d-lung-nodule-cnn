function vol_slic_train_3D(id)
N = length(id);
p1 = 1e2;
p2 = 10;
Data = dir('scan_proc/');
Data = Data(arrayfun(@(x) x.name(1), Data) ~= '.');

for n= 1:N
    filepath = fullfile('scan_proc',Data(id(n)).name);
    CT = load(filepath);
    labelpath = fullfile('3D-labels-gen',Data(id(n)).name);
    LP = load(labelpath);
    lab = LP.segments;
    
    loc = CT.scan.idx;
    img = CT.scan.img(:,:,loc(3)-5:loc(3)+5);
    lab = lab(:,:,loc(3)-5:loc(3)+5);
    
    disp(loc)
    imz = zeros(size(img));
    
    parfor i = 1:size(img,3)
        I = img(:,:,i);
        im = single(I)>500;
        imask = im_filter_seg(I,20,15);
        imz(:,:,i) = im.*imask;
        %imz(:,:,i) = im;
    end
    tens = single(img).*imz;
    [tmp slicids] = slicsupervoxelmex(tens,p1,p2);
    imagesc(drawregionboundaries(tmp(:,:,5), uint32(tens(:,:,5)),5000))
    pause()
    % tmp = vl_slic(single(tens),p1,p2);
    [segments,labels] = super_pixel_trainfeatures3D(tens,tmp,imz,lab);
    disp('Computed superpixel features, saving..')
    save(fullfile('spx-features-train',Data(id(n)).name),'segments','labels');
end
