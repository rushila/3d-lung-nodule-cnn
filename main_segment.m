clc
clear
rng('default');

whichdata = 'scan_train_proc';
numTrain = 30;
w = 25;
h = 4;

Data = dir(whichdata);
Data = Data(arrayfun(@(x) x.name(1), Data) ~= '.');

ids = 2:2:28;

%%-------------Training-----------------------
%create_training_data(10,w,h)
[feats,labels] = prepare_train(2:2:10,w,h);
imdb = create_imdb_matconvnet(feats,labels);
%%
net = custom_cnn_train(imdb);
%%-------------Testing------------------------
for i = 1:47
    p = prediction_main(i);
end
[tp,fn,fp]= compute_froc();