function [out,newl] = super_pixel_trainfeatures3D(img,segs,varargin)
tic
imz= varargin{1};
segs2 = single(segs).*imz;
Y = segs2(:);
Z = img(:);

if ~isempty(varargin{2})
    label = varargin{2};
    L = label(:);
else 
    L = zeros(1,length(Y));
end

idx = Y==0;
Y(idx) = [];
Z(idx) = [];
L(idx) = [];


[S, iSorted] = sort(Y);
markers = [0; find(diff(S)); numel(Y)];
bins = 10;
h = zeros(bins,numel(markers)-1);

for i = 1:numel(markers)-1
    
    f = Z(iSorted(markers(i)+1:markers(i+1)));
    h(:,i) = hist(f,bins);
    l(i) = nnz(L(iSorted(markers(i)+1:markers(i+1))))>0;
    
end
idx2 = l==0;
if ~nnz(l)
    error('No superpixels with nodule!')
end
negl = l(idx2);
l(idx2) = [];
ridx2 = randperm(sum(idx2),100);

newl = [l zeros(1,100)] ;
negh = h(:,idx2);
h(:,idx2) = [];
newh = [h negh(:,ridx2)];

out = newh;
toc
% for n = 1:length(superpixels)
%     tic
%     idx = Y == superpixels(n);
%     f = Z(idx);
%     toc
% end
