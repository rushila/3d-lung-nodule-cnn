function out = gen_proposal(segments,filename)
P = load(fullfile('results',filename));
out = segments.*(P.segments>0.3);